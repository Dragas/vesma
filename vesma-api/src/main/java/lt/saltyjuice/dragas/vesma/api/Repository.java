package lt.saltyjuice.dragas.vesma.api;

import java.util.Collection;

public interface Repository {
    Collection<Transaction> getAllTransactions();
}
