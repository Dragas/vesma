package lt.saltyjuice.dragas.vesma.api;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface Service {
    BigDecimal getTotalRevenue();

    Long getUniqueCustomerCount();

    String getMostPopularItem();

    LocalDate getHighestRevenueDay();
}
