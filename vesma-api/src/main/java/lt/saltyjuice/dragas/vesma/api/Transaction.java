package lt.saltyjuice.dragas.vesma.api;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public interface Transaction {
    Long getTransactionId();

    String getCustomerId();

    String getItemId();

    LocalDate getTransactionDate();

    BigDecimal getItemPrice();

    BigInteger getItemQuantity();


}
