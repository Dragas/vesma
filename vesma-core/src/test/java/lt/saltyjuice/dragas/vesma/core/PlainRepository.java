package lt.saltyjuice.dragas.vesma.core;

import lt.saltyjuice.dragas.vesma.api.Repository;
import lt.saltyjuice.dragas.vesma.api.Transaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;

public class PlainRepository implements Repository {
    @Override
    public Collection<Transaction> getAllTransactions() {
        return Arrays.asList(
                createTransaction(1L, "a", LocalDate.of(1, 1, 1), 1, 1),
                createTransaction(2L, "b", LocalDate.of(1, 1, 1), 2, 1),
                createTransaction(3L, "a", LocalDate.of(1, 1, 2), 1, 2),
                createTransaction(4L, "c", LocalDate.of(1, 1, 3), 3, 1)
        );
    }

    private Transaction createTransaction(Long transactionId, String customerId, LocalDate now, int quantity, int price) {
        PojoTransaction t = new PojoTransaction();
        t.setTransactionId(transactionId);
        t.setCustomerId(String.format("cust-%s", customerId));
        t.setItemId(String.format("item-%s", customerId));
        t.setTransactionDate(now);
        t.setItemQuantity(BigInteger.valueOf(quantity));
        t.setItemPrice(BigDecimal.valueOf(price));
        return t;
    }
}
