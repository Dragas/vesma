package lt.saltyjuice.dragas.vesma.core;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

class CoreServiceTest {

    private CoreService service;

    @BeforeEach
    public void setUp() {
        this.service = new CoreService(new PlainRepository());
    }

    @Test
    public void itReturnsTotalRevenue() {
        BigDecimal totalRevenue = service.getTotalRevenue();
        Assertions.assertEquals(BigDecimal.valueOf(8), totalRevenue);
    }

    @Test
    public void itReturnsUniqueCustomerCount() {
        Long count = service.getUniqueCustomerCount();
        Assertions.assertEquals(3L, count);
    }

    @Test
    public void itReturnsMostProfitableDay() {
        LocalDate date = service.getHighestRevenueDay();
        Assertions.assertEquals(LocalDate.of(1, 1,1), date);
    }

    @Test
    public void itReturnsMostPopularItem() {
        String item = service.getMostPopularItem();
        Assertions.assertEquals("item-c", item);
    }

    @AfterEach
    public void tearDown() {
    }
}