package lt.saltyjuice.dragas.vesma.core;

import lt.saltyjuice.dragas.vesma.api.Transaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public class PojoTransaction implements Transaction {
    private Long transactionId;
    private String customerId;
    private String itemId;
    private LocalDate transactionDate;
    private BigDecimal itemPrice;
    private BigInteger itemQuantity;

    @Override
    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @Override
    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public BigInteger getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(BigInteger itemQuantity) {
        this.itemQuantity = itemQuantity;
    }
}
