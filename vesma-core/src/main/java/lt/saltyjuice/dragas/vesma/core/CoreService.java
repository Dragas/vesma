package lt.saltyjuice.dragas.vesma.core;

import lt.saltyjuice.dragas.vesma.api.Repository;
import lt.saltyjuice.dragas.vesma.api.Service;
import lt.saltyjuice.dragas.vesma.api.Transaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class CoreService implements Service {
    private final Repository repository;

    public CoreService(Repository repository) {
        this.repository = repository;
    }

    @Override
    public BigDecimal getTotalRevenue() {
        BigDecimal revenue = this
                .repository
                .getAllTransactions()
                .stream()
                .map(this::getRevenueForTransaction)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        return revenue;
    }

    @Override
    public Long getUniqueCustomerCount() {
        Long uniqueCustomerCount = this
                .repository
                .getAllTransactions()
                .stream()
                .map(Transaction::getCustomerId)
                .distinct()
                .count();
        return uniqueCustomerCount;
    }

    @Override
    public String getMostPopularItem() {
        String mostPopularItem =
                repository
                        .getAllTransactions()
                        .stream()
                        .collect(
                                Collectors.groupingBy(
                                        Transaction::getItemId, Collectors.mapping(
                                                Transaction::getItemQuantity,
                                                Collectors.reducing(BigInteger::add)
                                        )
                                )
                        )
                        .entrySet()
                        .stream()
                        .reduce(
                                BinaryOperator.maxBy(
                                        Comparator.comparing(
                                                (it) -> it.getValue().orElse(BigInteger.ZERO),
                                                BigInteger::compareTo
                                        )
                                )
                        )
                        .map(Map.Entry::getKey)
                        .orElse("none");
        return mostPopularItem;
    }

    @Override
    public LocalDate getHighestRevenueDay() {
        LocalDate highestRevenueDate = repository
                .getAllTransactions()
                .stream()
                .collect(Collectors.groupingBy(
                        Transaction::getTransactionDate,
                        Collectors.mapping(this::getRevenueForTransaction, Collectors.reducing(BigDecimal::add))
                ))
                .entrySet()
                .stream()
                .reduce(BinaryOperator.maxBy(
                        Comparator.comparing(
                                (it) -> it.getValue().orElse(BigDecimal.ZERO),
                                BigDecimal::compareTo
                        )
                ))
                .map(Map.Entry::getKey)
                .orElse(null);
        return highestRevenueDate;
    }

    private BigDecimal getRevenueForTransaction(Transaction t) {
        return t
                .getItemPrice()
                .multiply(
                        BigDecimal.valueOf(
                                t.getItemQuantity()
                                        .longValue()
                        )
                );
    }
}
