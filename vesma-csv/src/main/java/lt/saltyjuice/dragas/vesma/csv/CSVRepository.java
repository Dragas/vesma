package lt.saltyjuice.dragas.vesma.csv;

import lt.saltyjuice.dragas.vesma.api.Repository;
import lt.saltyjuice.dragas.vesma.api.Transaction;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CSVRepository implements Repository {

    private final Collection<Transaction> transaction;

    public CSVRepository(String filename) throws IOException {
        File file = new File(filename);
        FileInputStream fin = new FileInputStream(file);
        InputStreamReader reader = new InputStreamReader(fin, StandardCharsets.UTF_8);
        Iterable<CSVRecord> transactionsCsv = CSVFormat.DEFAULT.builder().setHeader().setSkipHeaderRecord(true).build().parse(reader);
        Collection<Transaction> transactions = new ArrayList<>();
        for (CSVRecord transactionCsv : transactionsCsv) {
            Transaction transaction = new CSVTransaction(transactionCsv);
            transactions.add(transaction);
        }
        this.transaction = transactions;
    }
    @Override
    public Collection<Transaction> getAllTransactions() {
        return Collections.unmodifiableCollection(transaction);
    }
}
