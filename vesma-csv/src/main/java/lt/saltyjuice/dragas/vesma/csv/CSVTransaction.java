package lt.saltyjuice.dragas.vesma.csv;

import lt.saltyjuice.dragas.vesma.core.PojoTransaction;
import org.apache.commons.csv.CSVRecord;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public class CSVTransaction extends PojoTransaction {
    private static final String CUSTOMER_ID = "customer_id";
    private static final String TRANSACTION_ID = "transaction_id";
    private static final String ITEM_QUANTITY = "item_quantity";
    private static final String ITEM_PRICE = "item_price";
    private static final String TRANSACTION_DATE = "transaction_date";
    private static final String ITEM_ID = "item_id";
    private final CSVRecord transactioncsv;

    public CSVTransaction(CSVRecord transactionCsv) {
        this.transactioncsv = transactionCsv;
    }

    @Override
    public Long getTransactionId() {
        return Long.parseLong(transactioncsv.get(TRANSACTION_ID));
    }

    @Override
    public String getCustomerId() {
        return transactioncsv.get(CUSTOMER_ID);
    }

    @Override
    public String getItemId() {
        return transactioncsv.get(ITEM_ID);
    }

    @Override
    public LocalDate getTransactionDate() {
        return LocalDate.parse(transactioncsv.get(TRANSACTION_DATE));
    }

    @Override
    public BigDecimal getItemPrice() {
        return new BigDecimal(transactioncsv.get(ITEM_PRICE));
    }

    @Override
    public BigInteger getItemQuantity() {
        return new BigInteger(transactioncsv.get(ITEM_QUANTITY));
    }
}
